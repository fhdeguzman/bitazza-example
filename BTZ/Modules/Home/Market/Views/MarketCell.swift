//
//  MarketCell.swift
//  BTZ
//
//  Created by Franz Henri De Guzman  on 7/8/21.
//

import UIKit

class MarketCell: UITableViewCell {

    static let identifier = "MarketCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var currentPriceLabel: UILabel!
    @IBOutlet weak var previousPriceLabel: UILabel!
    @IBOutlet weak var changePercentageLabel: UILabel!
    
    @IBOutlet weak var backgroundContentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        resetUI()
        
        changePercentageLabel.layer.cornerRadius = 3
        changePercentageLabel.layer.masksToBounds = true
        
        backgroundContentView.layer.cornerRadius = 3
    }
    
    func resetUI() {
        titleLabel.text = ""
        subtitleLabel.text = ""
        currentPriceLabel.text = ""
        previousPriceLabel.text = ""
        changePercentageLabel.text = ""
        changePercentageLabel.backgroundColor = .clear
    }

    func configure(model: Instrument) {

        titleLabel.text = model.symbol
        
        if let market = model.market {
            if let lastTradeTime = market.lastTradeTime {
                let epochTime = TimeInterval(lastTradeTime) / 1000
                let date = Date(timeIntervalSince1970: epochTime)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/YY hh:mm a"
                subtitleLabel.text = dateFormatter.string(from: date)
            }
            if let lastTradePx = market.lastTradedPx {
                currentPriceLabel.text = String(lastTradePx).maxLength(length: 12)
            }
            
            if let bestOffer = market.bestOffer {
                previousPriceLabel.text = String(bestOffer).maxLength(length: 12)
            }
            
            if let currentDayPxChange = market.currentDayPxChange {
                changePercentageLabel.isHidden = false
                changePercentageLabel.text = String(format: "%.2f%", currentDayPxChange)
                
                if currentDayPxChange < 0 {
                    changePercentageLabel.backgroundColor = Color.RED
                }
                else {
                    changePercentageLabel.backgroundColor = Color.GREEN
                }
                
            }
            else {
                changePercentageLabel.isHidden = true
            }

        }
    }

}
