//
//  MarketViewController.swift
//  BTZ
//
//  Created by Franz Henri De Guzman  on 7/8/21.
//

import UIKit

/*
 We need to get the list of instruments by calling GetInstruments
 We will then use the list of instrument ids to subscribe to its market data to update the ui
 */
class MarketViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        WebsocketManager.shared.delegate = self
        
        fetchData()
        setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        WebsocketManager.shared.delegate = self
        tableView.reloadData()
    }
    

    func fetchData() {
        WebsocketManager.shared.getInstruments()
    }
    
    func setupRX() {
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
        
        SessionManager.current.instruments
            .bind(to: tableView.rx.items(cellIdentifier: MarketCell.identifier, cellType: MarketCell.self)) { (idx, item, cell) in
                
                cell.configure(model: item)
                
            }.disposed(by: disposeBag)
    }
    
    // Outlets
    @IBAction func logOutButtonTapped(_ sender: Any) {
        WebsocketManager.shared.logout()
    }
    
}


extension MarketViewController: WebsocketManagerDelegate {

    func didGetInstruments(_ data: Array<[String : Any]>) {
        
        for instrument in SessionManager.current.instruments.value {
            if let id = instrument.instrumentId {
                WebsocketManager.shared.updateMarketData(instrumentID: id)
            }
        }
    }
    
    func didLogout(_: Bool) {
        WebsocketManager.shared.unsubscribeInstruments(instruments: SessionManager.current.instruments.value)
        self.dismiss(animated: true, completion: {
            WebsocketManager.shared.closeConnection()
        })
    }

    func didSocketClosed(_ error: String) {
        let alert = UIAlertController(title: nil, message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { action in
            switch action.style{
                case .default:
                print("default")

                case .cancel:
                print("cancel")

                case .destructive:
                print("destructive")
                self.dismiss(animated: true, completion: nil)

                @unknown default:
                    break
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}


extension MarketViewController: UITableViewDelegate {
    
}

