//
//  BaseViewController.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/11/21.
//

import UIKit
import RxSwift
import RxCocoa
import ReSwift
import Starscream
import SwiftyJSON

class BaseViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


}
