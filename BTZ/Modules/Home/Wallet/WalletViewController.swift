//
//  WalletViewController.swift
//  BTZ
//
//  Created by Franz Henri De Guzman  on 7/8/21.
//

import UIKit

class WalletViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WebsocketManager.shared.delegate = self
        fetchData()
        setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        WebsocketManager.shared.delegate = self
        tableView.reloadData()
    }

    func fetchData() {
        WebsocketManager.shared.getUserAccounts()
    }
    
    func setupRX() {
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
        
        SessionManager.current.positions
            .bind(to: tableView.rx.items(cellIdentifier: WalletCell.identifier, cellType: WalletCell.self)) { (idx, item, cell) in
                
                cell.configure(model: item)
                
            }.disposed(by: disposeBag)
    }
    
}

extension WalletViewController: WebsocketManagerDelegate {
    
    func didUserAccounts(_ data: [Int]) {
        WebsocketManager.shared.getAccountPositions()
    }
    
    func didGetAccountsPosition(_ data: Array<[String:Any]>) {
        
        var positions = SessionManager.current.positions.value
        if (positions.count != 0) {
            positions.append(contentsOf: parsePositionData(data: data))
            SessionManager.current.positions.accept(positions)
        }
        else {
            SessionManager.current.positions.accept(parsePositionData(data: data))
        }
    }

    func didSocketClosed(_ error: String) {
        let alert = UIAlertController(title: nil, message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { action in
            switch action.style{
                case .default:
                print("default")

                case .cancel:
                print("cancel")

                case .destructive:
                print("destructive")
                self.dismiss(animated: true, completion: nil)

                @unknown default:
                    break
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension WalletViewController: UITableViewDelegate {
    
}
