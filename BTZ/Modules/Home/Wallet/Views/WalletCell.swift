//
//  WalletCell.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/11/21.
//

import UIKit

class WalletCell: UITableViewCell {

    static let identifier = "WalletCell"
    
    @IBOutlet weak var backgroundContentView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var fiatLabel: UILabel!
    @IBOutlet weak var cryptoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        resetUI()
        backgroundContentView.layer.cornerRadius = 3
    }

    func resetUI() {
        title.text = ""
        fiatLabel.text = ""
        cryptoLabel.text = ""
    }

    func configure(model: Position) {
        title.text = model.productSymbol
        fiatLabel.text = String(model.hold ?? 0)
        cryptoLabel.text = String(model.amount ?? 0) + " \(model.productSymbol)"
    }
}
