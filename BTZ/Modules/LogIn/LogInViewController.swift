//
//  LogInViewController.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/2/21.
//

import UIKit

class LogInViewController: BaseViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var signinBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!

    
    //MARK: Use authentication if we have a session token to avoid typing of credentials again
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextfield.text = "franzhenri.deguzman@gmail.com"
        passwordTextfield.text = "Dfr@nzh3nr1"
        
        signinBtn.backgroundColor = Color.GREEN
        signinBtn.layer.cornerRadius = 3
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        WebsocketManager.shared.delegate = self
    }
    
    func authenticateUser() {
        WebsocketManager.shared.authenticateUser(userName: emailTextfield.text!, password: passwordTextfield.text!)
    }
    
    @IBAction func signIn(_ sender: Any) {
        WebsocketManager.shared.establishConnection()
    }
    
    @IBAction func signUp(_ sender: Any) {
        
    }
    @IBAction func forgotPassword(_ sender: Any) {
        
    }
}


extension LogInViewController: WebsocketManagerDelegate {
    
    func didEstablishedConnection(_: Bool) {
        self.authenticateUser()
    }

    func didAuthenticate(_ data: [String:Any]) {
        let isAuthenticated: Bool = data["Authenticated"] as! Bool
        if isAuthenticated {
            SessionManager.newSession(data: data)
            if let tabViewController = storyboard!.instantiateViewController(withIdentifier: "HomeTabBarController") as? HomeTabBarController {
                tabViewController.modalPresentationStyle = .fullScreen
                present(tabViewController, animated: false, completion: nil)
            }
        }else{
            print(data["errormsg"] as! String)
        }
    }
}

