//
//  ParserManager.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/2/21.
//

import Foundation

func parseInstruments(data: Array<[String:Any]>) -> [Instrument] {
    
    var instruments = [Instrument]()
    
    for item in data {
        let instrument = Instrument()
        instrument.market = nil
        
        if let rawValue = item["InstrumentId"] as? Int {
            instrument.instrumentId = rawValue
        }
        
        if let rawValue = item["Symbol"] as? String {
            instrument.symbol = rawValue
        }
        
        if let rawValue = item["Product1Symbol"] as? String {
            instrument.product1Symbol = rawValue
        }
        
        instruments.append(instrument)
    }

    return instruments
}


func parseMarketData(data: [String:Any]) -> Market {
    
    let market = Market()
    
    if let rawValue = data["InstrumentId"] as? Int {
        market.instrumentId = rawValue
    }
    
    if let rawValue = data["SessionOpen"] as? Double {
        market.sessionOpen = rawValue
    }
    
    if let rawValue = data["CurrentDayPxChange"] as? Double {
        market.currentDayPxChange = rawValue
    }
    
    if let rawValue = data["LastTradedPx"] as? Double {
        market.lastTradedPx = rawValue
    }
    
    if let rawValue = data["BestOffer"] as? Double {
        market.bestOffer = rawValue
    }

    if let rawValue = data["LastTradeTime"] as? Double {
        market.lastTradeTime = rawValue
    }
    
    return market
}


func parsePositionData(data: Array<[String:Any]>) -> [Position] {
    
    var positions = [Position]()
    
    for positionRaw in data {
        let position = Position()
        
        if let rawValue = positionRaw["ProductSymbol"] as? String {
            position.productSymbol = rawValue
        }
        
        if let rawValue = positionRaw["AccountId"] as? Int {
            position.accountId = rawValue
        }
        
        if let rawValue = positionRaw["Hold"] as? Double {
            position.hold = rawValue
        }
        
        if let rawValue = positionRaw["Amount"] as? Double {
            position.amount = rawValue
        }
        
        if let rawValue = positionRaw["ProductId"] as? Int {
            position.productId = rawValue
        }
        
        if let rawValue = positionRaw["ProductType"] as? Int {
            position.productType = rawValue
        }
     
        positions.append(position)
    }
    
    return positions
}



func parseUserData(data: [String:Any]) -> User {
    
    let user = User()
    
    if let rawValue = data["UserId"] as? Int {
        user.userID = rawValue
    }
    
    if let rawValue = data["SessionToken"] as? String {
        user.sessionToken = rawValue
    }
    
    if let rawValue = data["Authenticated"] as? Bool {
        user.authenticated = rawValue
    }
    
    return user
}
