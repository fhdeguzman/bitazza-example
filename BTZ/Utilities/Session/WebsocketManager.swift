//
//  WebsocketManager.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/9/21.
//

import Foundation
import Starscream
import SwiftyJSON


protocol WebsocketManagerDelegate: AnyObject {
    func didEstablishedConnection(_ : Bool)
    func didCloseConnection(_ : Bool)
    func didLogout(_ : Bool)
    func didAuthenticate(_ data: [String:Any])
    func didUpdateMarketData(_ data: [String:Any])
    func didGetInstruments(_ data: Array<[String:Any]>)
    func didUserAccounts(_ data: [Int])
    func didGetAccountsPosition(_ data: Array<[String:Any]>)
    func didReceivedData(_ : Data)
    func didSocketClosed(_ error : String)
}

extension WebsocketManagerDelegate {
    func didEstablishedConnection(_ : Bool) {}
    func didCloseConnection(_ : Bool) {}
    func didLogout(_ : Bool) {}
    func didAuthenticate(_ data: [String:Any]) {}
    func didUpdateMarketData(_ data: [String:Any]) {}
    func didGetInstruments(_ data: Array<[String:Any]>) {}
    func didUserAccounts(_ data: [Int]) {}
    func didGetAccountsPosition(_ data: Array<[String:Any]>) {}
    func didReceivedData(_ : Data) {}
    func didSocketClosed(_ error : String) {}
}

final class WebsocketManager: NSObject, WebSocketDelegate {

    weak var delegate: WebsocketManagerDelegate?
    static let shared = WebsocketManager()

    var isConnected = Bool()
    var socket: WebSocket!

    override init() {
        super.init()
    }


    //MARK: Session services
    
    func establishConnection() {
        var request = URLRequest(url: URL(string: APIEndpoint.BASE_URL)!)
        request.timeoutInterval = 5
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }

    func closeConnection() {
        var newValue = SessionManager.current.instruments.value
        newValue.removeAll()
        SessionManager.current.instruments.accept(newValue)
        socket.disconnect()
    }

    func logout() {
        let param = [
            "m":0,
            "i":2,
            "n":APIEndpoint.LOGOUT
        ] as [String : Any]

        let paramData = try? JSONSerialization.data(withJSONObject: param, options: [])
        let paramStr = String(data: paramData!, encoding: String.Encoding.utf8)
        socket.write(string: paramStr!)
    }
    
    func authenticateUser(userName: String, password: String) {
        let param = ["Password": password,
                     "UserName": userName
        ]

        socket.write(string: generatePayload(param: param, endpoint: APIEndpoint.AUTH))
    }

    //MARK: Market services
    
    func getInstruments() {
        let param = ["OMSid": 1,
        ]

        socket.write(string: generatePayload(param: param, endpoint: APIEndpoint.GET_INSTRUMENTS))
    }
        
    func updateMarketData(instrumentID:Int) {

        let param = ["OMSid": 1,
                    "InstrumentId": instrumentID
        ]
        
        socket.write(string: generatePayload(param: param, endpoint: APIEndpoint.SUBSCRIBE_LEVEL1))
    }
    
    func unsubscribeInstruments(instruments: [Instrument]) {
        for instrument in instruments {
            let param = ["OMSid": 1,
                         "InstrumentId": instrument.instrumentId
            ]
            socket.write(string: generatePayload(param: param as [String : Any], endpoint: APIEndpoint.UNSUBSCRIBE_LEVEL1))
        }
    }
    
    //MARK: Wallet services

    func getUserAccounts() {
        let param = ["OMSid": 1,
                     "userId":SessionManager.current.user.userID!,
                     "userName" : ""
        ] as [String : Any]

        socket.write(string: generatePayload(param: param as [String : Any], endpoint: APIEndpoint.GET_USER_ACCOUNTS))
    }
    
    func getAccountPositions() {
        
        if let accounts = SessionManager.current.user.accounts {
            for accountId in accounts {
                let param = ["OMSid": 1,
                             "AccountId":accountId
                ]

                socket.write(string: generatePayload(param: param as [String : Any], endpoint: APIEndpoint.GET_ACCOUNTS_POSITION))
            }
        }
    }

    // MARK: - WebSocketDelegate
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
            case .connected(let headers):
                isConnected = true
                print("websocket is connected: \(headers)")
                delegate?.didEstablishedConnection(true)
            case .disconnected(let reason, let code):
                isConnected = false
                print("websocket is disconnected: \(reason) with code: \(code)")
            case .text(let string):
                print("Received text: \(string)")

                do {
                    if let json = try JSONSerialization.jsonObject(with: Data(string.utf8), options: []) as? [String: Any] {
                        let responsePayload: String = json["o"] as! String
                        
                        let event: String = json["n"] as! String
                        
                        if event == APIEvent.GET_INSTRUMENTS || event == APIEvent.GET_ACCOUNTS_POSITION {
                            do {
                                if let response = try JSONSerialization.jsonObject(with: Data(responsePayload.utf8), options: []) as? Array<[String:Any]> {
                                    if event == APIEvent.GET_INSTRUMENTS {
                                        SessionManager.current.instruments.accept(parseInstruments(data: response))
                                        delegate?.didGetInstruments(response)
                                    }else if event == APIEvent.GET_ACCOUNTS_POSITION {
                                        delegate?.didGetAccountsPosition(response)
                                    }
                                }
                                
                            } catch let error {
                                print("json error: \(error)")
                            }
                        }else if event == APIEvent.GET_USER_ACCOUNTS {
                            do {
                                if let response = try JSONSerialization.jsonObject(with: Data(responsePayload.utf8), options: []) as? [Int] {
                                    if event == APIEvent.GET_USER_ACCOUNTS {
                                        SessionManager.current.user.accounts = response
                                        delegate?.didUserAccounts(response)
                                    }
                                }

                            } catch let error {
                                print("json error: \(error)")
                            }
                        } else if let response = try JSONSerialization.jsonObject(with: Data(responsePayload.utf8), options: []) as? [String: Any] {
                            if event == APIEvent.AUTH {
                                delegate?.didAuthenticate(response)
                            }else if event == APIEvent.SUBSCRIBE_LEVEL1 {
                                
                                let marketData = parseMarketData(data: response)
                                
                                let newValue = SessionManager.current.instruments.value
                                newValue.filter {$0.instrumentId == marketData.instrumentId}.first?.market = marketData
                                SessionManager.current.instruments.accept(newValue)
                                
                            }else if event == APIEvent.UNSUBSCRIBE_LEVEL1 {
                                print("unsubscribed")
                            }else if event == APIEvent.LOGOUT {
                                delegate?.didLogout(true)
                            }
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                
            case .binary(let data):
                print("Received data: \(data.count)")
            case .ping(_):
                break
            case .pong(_):
                break
            case .viabilityChanged(_):
                break
            case .reconnectSuggested(_):
                break
            case .cancelled:
                isConnected = false
            case .error(let error):
                isConnected = false
                handleError(error)
        }
    }

    func handleError(_ error: Error?) {
        if let e = error as? WSError {
            print("websocket encountered an error: \(e.message)")
            delegate?.didSocketClosed(e.message)
        } else if let e = error {
            print("websocket encountered an error: \(e.localizedDescription)")
            delegate?.didSocketClosed(e.localizedDescription)
        } else {
            print("websocket encountered an error")
            delegate?.didSocketClosed("encountered an error")
        }
    }
}

// MARK: Helpers
extension WebsocketManager {
    
    func generatePayload(param: [String: Any], endpoint: String) -> String {
        let c = try? JSONSerialization.data(withJSONObject: param, options: [])
        let cstr = String(data: c!, encoding: String.Encoding.utf8)

        let payload = [
            "m":0,
            "i":2,
            "n":endpoint,
            "o": cstr!
        ] as [String : Any]

        let data = try? JSONSerialization.data(withJSONObject: payload, options: [])
        let str = String(data: data!, encoding: String.Encoding.utf8) ?? ""
        
        return str
    }
}
