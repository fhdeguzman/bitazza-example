//
//  SessionManager.swift
//  BTZ
//
//  Created by Franz Henri De Guzman  on 7/10/21.
//

import Foundation
import RxSwift
import RxCocoa

class SessionManager {
    
    static let current = SessionManager()

    var user = User()
    var instruments: BehaviorRelay<[Instrument]> = BehaviorRelay(value: [])
    var positions: BehaviorRelay<[Position]> = BehaviorRelay(value: [])
    
    static func newSession(data: [String:Any]) {
        current.user = parseUserData(data: data)
    }
}
