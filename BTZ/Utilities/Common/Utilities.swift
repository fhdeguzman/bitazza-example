//
//  Utilities.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/3/21.
//

import Foundation
import SystemConfiguration
import Network

class InternetConnection {

    static func checkNetworkConnectivity(completion: @escaping (_ result: Bool) -> Void) {

        DispatchQueue.global(qos: .background).async {

            let urls = [
                "https://bing.com",
                "https://google.com",
                "https://baidu.com",
                "https://apple.com",
                "https://yahoo.com"
            ]

            if  UserDefaults.standard.bool(forKey: "networkchecked") && UserDefaults.standard.object(forKey: "networkchecked") != nil {
                UserDefaults.standard.set(false, forKey: "networkchecked")
                var isConnected = Bool()

                for urlPath in urls {
                    print(urlPath)


                    let url = NSURL(string: urlPath)
                    let request = NSMutableURLRequest(url: url! as URL)
                    request.httpMethod = "HEAD"
                    request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
                    request.timeoutInterval = 5.0

                    var response:URLResponse?

                    do {
                        let _ = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        continue
                    }

                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            isConnected = true
                            break
                        }
                    }
                }

                if isConnected {
                    UserDefaults.standard.set(true, forKey: "networkchecked")
                    completion(true)
                }else{
                    UserDefaults.standard.set(true, forKey: "networkchecked")
                    completion(false)
                }
            }else{
                return
            }
        }

    }
}

class Utilities {
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }

    @available(iOS 12.0, *)
    static let monitor = NWPathMonitor(requiredInterfaceType: .wifi)
    static let queue = DispatchQueue.global(qos: .background)
    static var monitorStarted: Bool = false

    static func isOnWifi(_ idFromTimer: Bool = false, completion: @escaping (_ result: Bool) -> Void) {
        if #available(iOS 12.0, *) {
            //if !monitorStarted {
            monitorStarted = true
            monitor.start(queue: queue)
            //}
        } else { }

        if #available(iOS 12.0, *) {
            monitor.pathUpdateHandler = { path in
                DispatchQueue.main.async {
                    if path.status == .satisfied {
                        print("Connected")
                        if !idFromTimer {
                            print("NO INTERNET")
                        }
                        completion(true)
                    } else {
                        completion(false)
                    }
                }
            }
        } else {
            completion(currentReachabilityStatus == .reachableViaWiFi)
        }
    }

    static var isChecking: Bool = false
    static var currentReachabilityStatus: ReachabilityStatus? {

        let currentWifiConnection = self.currentWifiConnection

        guard currentWifiConnection != .notReachable else {
            DispatchQueue.main.async {
                print("NO INTERNET")
            }
            return .notReachable
        }
        return .reachableViaWiFi
    }


    static var currentWifiConnection: ReachabilityStatus {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }

        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
}


extension String {
   func maxLength(length: Int) -> String {
       var str = self
       let nsString = str as NSString
       if nsString.length >= length {
           str = nsString.substring(with:
               NSRange(
                location: 0,
                length: nsString.length > length ? length : nsString.length)
           )
       }
       return  str
   }
}
