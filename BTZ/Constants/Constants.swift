//
//  Constants.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/2/21.
//

import Foundation
import UIKit


struct APIEndpoint {
    static let BASE_URL = "wss://apexapi.bitazza.com/WSGateway/"
    static let AUTH = "WebAuthenticateUser"
    static let AUTH2FA = "Authenticate2FA"
    static let SUBSCRIBE_LEVEL1 = "SubscribeLevel1"
    static let UNSUBSCRIBE_LEVEL1 = "UnsubscribeLevel1"
    static let GET_INSTRUMENTS = "GetInstruments"
    static let GET_ACCOUNTS_POSITION = "GetAccountPositions"
    static let GET_USER_ACCOUNTS = "GetUserAccounts"
    static let GET_PRODUCTS = "GetProducts"
    static let LOGOUT = "LogOut"
}

struct APIEvent {
    static let AUTH = "WebAuthenticateUser"
    static let AUTH2FA = "Authenticate2FA"
    static let SUBSCRIBE_LEVEL1 = "Level1UpdateEvent"
    static let UNSUBSCRIBE_LEVEL1 = "Level1UpdateEvent"
    static let GET_INSTRUMENTS = "GetInstruments"
    static let GET_ACCOUNTS_POSITION = "GetAccountPositions"
    static let GET_USER_ACCOUNTS = "GetUserAccounts"
    static let GET_PRODUCTS = "GetProducts"
    static let LOGOUT = "LogOut"
}

struct Color {
    static let GREEN = UIColor(red: 50/255.0, green: 168/255.0, blue: 82/255.0, alpha: 1.0)
    static let RED = UIColor(red: 204/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
}
