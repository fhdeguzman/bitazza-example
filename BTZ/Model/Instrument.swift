//
//  Instrument.swift
//  BTZ
//
//  Created by Franz Henri De Guzman  on 7/9/21.
//

import Foundation

class Instrument: NSObject {
    
    var instrumentId: Int?
    var symbol = ""
    var product1Symbol = ""
    var market: Market?

}
