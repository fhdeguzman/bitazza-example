//
//  Market.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/2/21.
//

import Foundation

class Market: NSObject {
    
    var instrumentId: Int?
    var sessionOpen: Double?
    var currentDayPxChange: Double?
    var lastTradedPx: Double?
    var lastTradeTime: Double?
    var bestOffer: Double?
    
}
