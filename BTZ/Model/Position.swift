//
//  Position.swift
//  BTZ
//
//  Created by Franz Henri De Guzman  on 7/11/21.
//

import Foundation

class Position: NSObject {
    
    var productSymbol = ""
    var accountId: Int?
    var amount: Double?
    var hold: Double?
    var productId: Int?
    var productType: Int?
}
