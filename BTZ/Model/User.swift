//
//  User.swift
//  BTZ
//
//  Created by Franz Henri De Guzman on 7/3/21.
//

import Foundation

class User: NSObject {
    
    var userID: Int?
    var sessionToken: String?
    var authenticated: Bool?
    var accounts: [Int]?

}


